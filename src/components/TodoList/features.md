# Lista de features:

1. Usuário precisa ser capaz de adicionar novos todos escrevendo o texto no campo de texto e clicando em "Add". O novo todo será mostrado na lista abaixo. Não pode ser possível adicionar um todo sem texto.
2. Usuário precisa ser capaz de limpar todos os itens da lista que já estão completos clicando no botão "Clear Completed" que ficará do lado direito do botão "Add".
3. Cada item da lista de todos precisa conter um botão "delete", ao clicar nesse botão o todo será removido da lista.
4. Cada item da lista de todos precisa ter um checkbox que será usado para marcar o todo como "completed".
5. Caso o checkbox esteja selecionado o texto do todo precisa ter um risco no meio. Ex: ~~Todo Completo~~
6. Os todos devem vir da seguinte URL: https://jsonplaceholder.typicode.com/todos

A imagem abaixo é o resultado final esperado.
![exemplo](./exemplo.png) 