import React from 'react';

type Todo = {
  id: number;
  userId: number;
  title: string;
  completed: boolean;
};

const TodoList: React.FC = () => {
  // Adicionar novo todo na lista
  const addTodo = () => {
  };

  // Remove todo da lista
  const removeTodo = () => {
  };

  // Marca todo como completo ou incompleto
  const toggleTodo = (): void => {
  };

  // Limpa todos os todos completos
  const clearCompleted = (): void => {
  };

  return (
    <div className="todo-app">
      <h1>Todo List</h1>
      <input
        type="text"
        placeholder="Add a todo"
        className="todo-input"
      />
      <div className='button-container'>
        <button className="todo-button">Add</button>
        <button className="todo-button">Clear Completed</button>
      </div>
      <ul className="todo-list">
      </ul>
    </div>
  );
};

export default TodoList;
