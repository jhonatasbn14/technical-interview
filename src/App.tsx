import './styles/global.css'
import TodoList from './components/TodoList/TodoList'

function App() {
  return (
    <TodoList />
  )
}

export default App
